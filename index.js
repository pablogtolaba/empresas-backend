import express from 'express';
import cors from 'cors';
import router from './routes.js';

const aplicacion = express(); // creamos la aplicacion usando express

aplicacion.use(express.json()); // middleware para convertir los objectos JSON en objectos Javascript
aplicacion.use(cors());
aplicacion.use(router); // llamamos a las rutas

aplicacion.listen(3000, () => {
    console.log("El servidor esta escuchando en el puerto 3000");
}); // levantamos la aplicacion en el puerto 3000