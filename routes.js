import { Router } from "express"
import { createEmpresa, deleteByIdEmpresas, getAllEmpresas, getByIdEmpresas, updateByIdEmpresas } from "./controllers/empresa.controller.js";
import { createSucursal, deleteSucursalById, getSucursalById, updateSucursalById } from "./controllers/sucursal.controller.js";

const router = Router();

router.get('/empresa', getAllEmpresas);
router.get('/empresa/id', getByIdEmpresas);
router.post('/empresa/create', createEmpresa);
router.patch('/empresa/update/:id', updateByIdEmpresas);
router.delete('/empresa/delete/:id', deleteByIdEmpresas);

router.get('/sucursal/:id', getSucursalById);
router.post('/sucursal', createSucursal);
router.patch('/sucursal/:id', updateSucursalById);
router.delete('/sucursal/:id', deleteSucursalById);

export default router;