import { Empresa, Sucursal } from "../models/index.js";

export const createEmpresa = async (request, response) => {
    try {

        const body = request.body;
    
        if (body.nombre == null) {
            response
                .status(400)
                .send({
                    mensaje: 'El nombre es obligatorio'
                });
    
            return;
        }
    
        const empresa = {
            nombre: body.nombre,
            cuit: body.cuit
        };
        await Empresa.create(empresa)

        response.send( { success: "Empresa creada con exito" } );
    } catch(error) {
        response
            .status(500)
            .send({
                mensaje: error.message || 'Algo salió mal'
            })
    }
};

export const getAllEmpresas = async (request, response) => {
    try {
        
        const data = await Empresa.findAll({ include: ["sucursales"] });
        
        if(data != null) {
            response.send(data)
        } else {
            response
                .status(404)
                .send({
                    mensaje: 'La empresa no existe'
                });
        }

    } catch (error) {
        throw error;
    }
};

export const getByIdEmpresas = async (request, response) => {
    try {
        const idEmpresa = request.query.id;
    
        const parametros = { 
            where: { 
                id: idEmpresa
            }, 
            include: ["sucursales"]
        }
    
        const empresa = await Empresa.findOne(parametros)
        if (empresa) {
            response.send(empresa)
        } else {
            response
            .status(404)
            .send({
                mensaje: 'La empresa no existe'
            })
        }

    } catch(error) {
        response
            .status(500)
            .send({
                mensaje: error.message || 'Algo salió mal'
            })
    }
};

export const updateByIdEmpresas = async (request, response) => {
    try {
        const empresaId = request.params.id;
    
        if (!empresaId || isNaN(empresaId)) {
            response
                .status(400)
                .send({
                    mensaje: 'El id es invalido'
                });
    
            return;
        }
    
        const empresaDB = await Empresa.findOne({ where: { id: empresaId}})
    
        if (!empresaDB) {
            errorHelper(response, 404, "La empresa no existe")
        }
    
        const body = request.body;
    
        const empresa = {}
    
        if (body.nombre != null) {
            if (typeof body.nombre == "string") {
                empresa.nombre = body.nombre;
            } else {
                errorHelper(response, 400, 'El nombre no es un String')
                return;
            }
        } 
    
        if (body.cuit != null) {
            if (typeof body.cuit == "string") {
                empresa.cuit = body.cuit;
            } else {
                errorHelper(response, 400, 'El cuit no es un String')
                return;
            }
        } 
    
        const filtro = {
            where: {
                id: empresaId
            }
        }
        
        await Empresa.update(empresa, filtro);
        
        response.send({mensaje: "El registro se actualizó con exito"})
    } catch (error) {
        errorHelper(response, 500, "error en el servidor");
    }
};

export const deleteByIdEmpresas = async (request, response) => {
    try {
        const empresaId = request.params.id;
    
        if (!empresaId || isNaN(empresaId)) {
            response
                .status(400)
                .send({
                    mensaje: 'El id es invalido'
                });
    
            return;
        }
        
        const filtro = { 
            where: { 
                id: empresaId
            }
        };
        
        const filtroSucursal = { 
            where: { 
                empresa_id: empresaId
            }
        };

        const empresaDB = await Empresa.findOne(filtro)
    
        if (!empresaDB) {
            errorHelper(response, 404, "La empresa no existe")
        }
        
        const sucursales = await Sucursal.findAll(filtroSucursal);

        if (sucursales && sucursales.length > 0) {
            await Sucursal.destroy(filtroSucursal);
        }

        await Empresa.destroy(filtro);
        
        response.send({mensaje: "El registro se actualizó con exito"})
    } catch (error) {
        errorHelper(response, 500, error.message);
    }
};

const errorHelper = (response, codigo, mensaje) => {
    response
        .status(codigo)
        .send({
            mensaje: mensaje
        });
}